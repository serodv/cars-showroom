package carshowroom.repository;

import carshowroom.model.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface HistoryRepository extends JpaRepository<History, Long>{
    @Query("SELECT count(h.car) FROM History h WHERE h.car.id = ?1")
    Integer existByCarId(Long carId);
    List<History> findAllBySalesPersonId(Long id);
    @Query("SELECT h FROM History h WHERE h.car.showRoom.id = ?1")
    List<History> findAllByShowRoomId(Long id);
}
