package carshowroom.repository;

import carshowroom.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface CarRepository extends JpaRepository<Car, Long> {
    @Query("SELECT c FROM Car c WHERE c.showRoom.id = ?1")
    List<Car> findByShowRoomId(Long id);
    List<Car> findAllByHistoryNull();
    List<Car> findAllByShowRoomIdAndHistoryNull(Long id);
}
