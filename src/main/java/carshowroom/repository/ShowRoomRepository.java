package carshowroom.repository;

import carshowroom.model.ShowRoom;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ShowRoomRepository extends JpaRepository<ShowRoom, Long>{
}
