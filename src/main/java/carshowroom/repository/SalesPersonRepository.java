package carshowroom.repository;

import carshowroom.model.SalesPerson;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface SalesPersonRepository extends JpaRepository<SalesPerson, Long> {
    Optional<SalesPerson> findOneByUsername(String username);
}
