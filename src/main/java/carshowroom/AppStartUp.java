package carshowroom;

import carshowroom.model.Car;
import carshowroom.model.History;
import carshowroom.model.SalesPerson;
import carshowroom.model.ShowRoom;
import carshowroom.repository.CarRepository;
import carshowroom.repository.HistoryRepository;
import carshowroom.repository.SalesPersonRepository;
import carshowroom.repository.ShowRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class AppStartUp implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private SalesPersonRepository salesPersonRepository;
    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private ShowRoomRepository showRoomRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Random random = new Random();
        List<String[]> lines = readFile("csv/showrooms.csv");
        List<ShowRoom> showRooms = new ArrayList<>();
        System.out.println("\nAvailable showrooms:");
        for (String[] line : lines) {
            ShowRoom showRoom = new ShowRoom(null, line[0], line[1], null, null);
            ShowRoom showRoomWithId = showRoomRepository.save(showRoom);
            System.out.println(showRoomWithId);
            showRooms.add(showRoomWithId);
        }

        System.out.println("\nAvailable sale persons:");
        lines = readFile("csv/salespersons.csv");
        List<SalesPerson> salesPeople = new ArrayList<>();
        List<ShowRoom> avShowRooms = new ArrayList<>(showRooms);
        for (String[] line : lines) {
            int loginPassSuffix = random.nextInt(100);
            SalesPerson salesPerson = new SalesPerson(null, line[0], line[1], line[2], null, line[3]+loginPassSuffix, passwordEncoder.encode(line[4]+loginPassSuffix));
            salesPerson.setShowRoom(new ArrayList<>(showRooms));
            SalesPerson salesPersonWithId = salesPersonRepository.save(salesPerson);
            System.out.println(salesPersonWithId + ". login/pass - " + line[3]+loginPassSuffix + "/" + line[4]+loginPassSuffix);
            salesPeople.add(salesPersonWithId);
            if (!showRooms.isEmpty()) {
                showRooms.remove(0);
            }
        }

        System.out.println("\nCars:");
        lines = readFile("csv/cars.csv");
        List<Car> cars = new ArrayList<>();
        for (String[] line : lines) {
            Car car = new Car(null, line[0], line[1], Integer.parseInt(line[2]), Double.parseDouble(line[3]), line[4], null, null);
            car.setShowRoom(avShowRooms.get(random.nextInt(avShowRooms.size())));
            Car carWithId = carRepository.save(car);
            System.out.println(car);
            cars.add(carWithId);
        }

        System.out.println("\nAvailable sale history:");
        for (SalesPerson salesPerson : salesPeople) {
            List<Long> ids = salesPerson.getShowRoom().stream().map(ShowRoom::getId).collect(Collectors.toList());
            Optional<Car> anyCar = cars.stream().filter(car -> car.getHistory()==null && ids.contains(car.getShowRoom().getId())).findAny();
            if (anyCar.isPresent()) {
                History history = new History();
                history.setDateTime(LocalDateTime.now());
                history.setCar(anyCar.get());
                history.setSalesPerson(salesPerson);
                historyRepository.save(history);
                System.out.println(history);
                cars.removeIf(car->car.getId().equals(anyCar.get().getId()));
            }
        }

    }

    private List<String[]> readFile(String fileName) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(fileName);
        InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);
        return reader.lines().map(line -> line.split(",")).collect(Collectors.toList());
    }
}
