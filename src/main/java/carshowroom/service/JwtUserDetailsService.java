package carshowroom.service;

import carshowroom.model.SalesPerson;
import carshowroom.repository.SalesPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    SalesPersonRepository salesPersonRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SalesPerson salesPerson = salesPersonRepository.findOneByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("Account not found"));
            return new User(username, salesPerson.getPassword(), new ArrayList<>());
        }
    }
