package carshowroom.service;

import carshowroom.exceptions.BusinessException;
import carshowroom.model.Car;
import carshowroom.model.History;
import carshowroom.model.SalesPerson;
import carshowroom.model.ShowRoom;
import carshowroom.model.dto.CarDTO;
import carshowroom.model.dto.SaleDTO;
import carshowroom.repository.CarRepository;
import carshowroom.repository.HistoryRepository;
import carshowroom.repository.SalesPersonRepository;
import carshowroom.repository.ShowRoomRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarsService {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private TypeMap<Car, CarDTO> typeMaptoDto;
    @Autowired
    private TypeMap<CarDTO, Car> typeMapToEnt;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private ShowRoomRepository showRoomRepository;
    @Autowired
    private SalesPersonRepository salesPersonRepository;
    @Autowired
    HistoryRepository historyRepository;

    public List<CarDTO> getAllCars() {
        return carRepository.findAll()
                .stream()
                .map(typeMaptoDto::map)
                .collect(Collectors.toList());
    }

    public List<CarDTO> getAllCarsByShowRoomId(Long id) {
        return carRepository.findByShowRoomId(id)
                .stream()
                .map(typeMaptoDto::map)
                .collect(Collectors.toList());
    }

    public CarDTO save(CarDTO carDTO) {
        carDTO.setId(null);
        Car car = typeMapToEnt.map(carDTO);
        ShowRoom showRoom = showRoomRepository.findById(carDTO.getShowRoom()).orElseThrow(BusinessException::new);
        car.setShowRoom(showRoom);
        return typeMaptoDto.map(carRepository.save(car));
    }

    public CarDTO update(CarDTO carDTO) {
        if (!carRepository.existsById(carDTO.getId())) {
            throw new BusinessException("car #" + carDTO.getId() + " not found");
        }
        Car car = typeMapToEnt.map(carDTO);
        ShowRoom showRoom = showRoomRepository.findById(carDTO.getShowRoom()).orElseThrow(BusinessException::new);
        car.setShowRoom(showRoom);
        return typeMaptoDto.map(carRepository.save(car));
    }

    public SaleDTO performSale(SaleDTO saleDTO) {
        Car car = carRepository.findById(saleDTO.getCarId())
                .orElseThrow(() -> new BusinessException("The car # " + saleDTO.getCarId() + " wasn't found"));
        SalesPerson salesPerson = salesPersonRepository.findById(saleDTO.getSalePersonId())
                .orElseThrow(() -> new BusinessException("The sale person # " + saleDTO.getSalePersonId() + " wasn't found"));

        if (historyRepository.existByCarId(saleDTO.getCarId()) > 0) {
            throw new BusinessException("The car # " + saleDTO.getCarId() + " already sold");
        }
        if (!salesPerson.getShowRoom().contains(car.getShowRoom())) {
            throw new BusinessException("The sale person # " + saleDTO.getSalePersonId() +
                    " doesn't have authority to sell car from showroom # " + car.getShowRoom().getId());
        }
        History history = History.builder()
                .car(car)
                .salesPerson(salesPerson)
                .dateTime(LocalDateTime.now())
                .build();

        History savedHistory = historyRepository.save(history);
        SaleDTO saleResponseDto = modelMapper.map(savedHistory, SaleDTO.class);
        saleResponseDto.setCarId(savedHistory.getCar().getId());
        saleResponseDto.setSalePersonId(savedHistory.getSalesPerson().getId());
        return saleResponseDto;
    }

    public List<Car> getAllAvailableCars() {
        return carRepository.findAllByHistoryNull();
    }

    public List<Car> getAllAvailableCarsByShowRoom(Long showRoomId) {
        return carRepository.findAllByShowRoomIdAndHistoryNull(showRoomId);
    }

    public List<History> getAllSales() {
        return historyRepository.findAll();
    }

    public List<History> getAllSalesBySalesPerson(Long id) {
        return historyRepository.findAllBySalesPersonId(id);
    }

    public List<History> getAllSalesByShowRoom(Long id) {
        return historyRepository.findAllByShowRoomId(id);
    }

}
