package carshowroom.model.dto;

import lombok.Data;
import java.time.LocalDateTime;

@Data
public class SaleDTO {
    private Long id;
    private Long salePersonId;
    private Long carId;
    private LocalDateTime dateTime;
}
