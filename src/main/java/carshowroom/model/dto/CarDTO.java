package carshowroom.model.dto;

import lombok.Data;

@Data
public class CarDTO {
        private Long id;
        private String manufacturer;
        private String model;
        private int milleage;
        private double price;
        private String options;
        private Long showRoom;
}
