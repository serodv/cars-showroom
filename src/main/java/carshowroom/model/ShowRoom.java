package carshowroom.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShowRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
    @JsonIgnore
    @ManyToMany(mappedBy = "showRoom")
    private List<SalesPerson> salePersons;
    @JsonIgnore
    @OneToMany(mappedBy = "showRoom")
    private List<Car> cars;

    @Override
    public String toString() {
        return "ShowRoom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
