package carshowroom.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalesPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String fullName;
    private String phone;
    private String email;
    @ManyToMany
    private List<ShowRoom> showRoom;
    @JsonIgnore
    private String username;
    @JsonIgnore
    private String password;

    @Override
    public String toString() {
        List<String> ids = showRoom.stream().map(ShowRoom::getId).map(String::valueOf).collect(Collectors.toList());
        return "SalesPerson{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", showRoom=" + String.join(",",ids) +
                '}';
    }
}
