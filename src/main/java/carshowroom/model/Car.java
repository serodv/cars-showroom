package carshowroom.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String manufacturer;
    private String model;
    private int milleage;
    private double price;
    private String options;
    @ManyToOne
    private ShowRoom showRoom;
    @JsonIgnore
    @OneToOne(mappedBy = "car")
    private History history;

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", milleage=" + milleage +
                ", price=" + price +
                ", options='" + options + '\'' +
                ", showRoom=" + showRoom.getId() +
                '}';
    }
}
