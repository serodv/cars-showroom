package carshowroom.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne
    private Car car;
    @OneToOne
    private SalesPerson salesPerson;
    @Column(columnDefinition="timestamp(6)")
    private LocalDateTime dateTime;

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", car=" + car.getId() +
                ", salesPerson=" + salesPerson.getId() +
                ", dateTime=" + dateTime +
                '}';
    }
}
