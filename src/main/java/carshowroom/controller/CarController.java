package carshowroom.controller;

import carshowroom.model.Car;
import carshowroom.model.dto.CarDTO;
import carshowroom.service.CarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/cars")
public class CarController {
    @Autowired
    private CarsService carsService;

    @PostMapping("/all")
    public ResponseEntity<List<CarDTO>> getAllCars() {
        return new ResponseEntity<>(carsService.getAllCars(), HttpStatus.OK);
    }

    @PostMapping("/{showRoomId}")
    public ResponseEntity<List<CarDTO>> getAllCarsByShowRoom(@PathVariable("showRoomId") Long showRoomId) {
        return new ResponseEntity<>(carsService.getAllCarsByShowRoomId(showRoomId), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<CarDTO> addCar(@RequestBody CarDTO carDto) {
        CarDTO save = carsService.save(carDto);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<CarDTO> updateCar(@RequestBody CarDTO carDto) {
        CarDTO save = carsService.update(carDto);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @PostMapping("/available/all")
    public List<Car> getAllAvailableCars() {
        return carsService.getAllAvailableCars();
    }

    @PostMapping("/available/{showRoomId}")
    public List<Car> getAllAvailableCarsByShowRoom(@PathVariable("showRoomId") Long showRoomId) {
        return carsService.getAllAvailableCarsByShowRoom(showRoomId);
    }

}
