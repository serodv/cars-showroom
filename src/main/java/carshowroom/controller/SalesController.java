package carshowroom.controller;


import carshowroom.model.History;
import carshowroom.model.dto.SaleDTO;
import carshowroom.service.CarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    CarsService carsService;

    @PostMapping("/makesale")
    public ResponseEntity<SaleDTO> performSale(@RequestBody SaleDTO saleDTO) {
        return ResponseEntity.ok(carsService.performSale(saleDTO));
    }

    @PostMapping("/all")
    public ResponseEntity<List<History>> allSales() {
        return ResponseEntity.ok(carsService.getAllSales());
    }

    @PostMapping("/salesperson/{id}")
    public ResponseEntity<List<History>> allSalesBySalesPerson(@PathVariable("id") Long id) {
        return ResponseEntity.ok(carsService.getAllSalesBySalesPerson(id));
    }

    @PostMapping("/showroom/{id}")
    public ResponseEntity<List<History>> allSalesByShowRoom(@PathVariable("id") Long id) {
        return ResponseEntity.ok(carsService.getAllSalesByShowRoom(id));
    }

}
